import React from 'react';
import gambar from './avenger.jpg';
export const Home = () => {
    return (
        <div style={{width:"1000px", margin:"20px auto"}}>
            <h1>Daftar FIlm Terbaik</h1>
            <img src={gambar} alt="gambar"/>
            <p> The most definitive overarching issue with the Marvel Cinematic Universe has been the lack of stakes. Over the course of the saga’s previous 18 movies, MCU heroes have faced numerous world-ending threats, eking out victories by the skin of their teeth, only to have their worlds essentially return to normal in time for the next installment. The approach worked early, on a film-by-film basis, but when viewed as part of a 10-year narrative, it’s tended to weaken the broader franchise. There can be no drama without true risk, and in the MCU, audiences have learned that none of their favorites are ever really in harm’s way.

Directors Joe and Anthony Russo seem acutely aware of this issue with their latest entry, the massive, multi-film team-up Avengers: Infinity War. The long-awaited face-off between the Avengers and Thanos (Josh Brolin), the MCU’s ultimate big bad, is massively entertaining, deftly incorporating dozens of characters across multiple storylines with a kinetic flair. Its devotion to banter and one-liners makes it one of the funniest movies in the studio’s history, but it’s also a film where very bad things happen to good people. After years of movies where even the most mediocre heroes appeared to be invulnerable and indomitable, it’s an arresting jolt — and exactly the film the franchise needed. </p>
        </div>
    )
}