import React from 'react';
import {
    Form,
    FormGroup,
    FormText,
    FormLabel,
    Button,
    Table
} from 'react-bootstrap';


export const Login = () => {
    return (
        <div className="form" style={{ width: "800px", border: "1px black solid", margin: "30px auto", padding: "20px 20px" }}>
            <Form text="left" >
                <h1> Movie Form</h1>
                <Form.Group controlId="formUser">
                    <Form.Label for="username">Username </Form.Label>
                    <Form.Control id="username" type="text" placeholder="Enter username" />

                </Form.Group>
                <Form.Group controlId="formPassword">
                    <Form.Label for="password">Password </Form.Label>
                    <Form.Control id="password" type="password" placeholder="enter password" />

                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Submit
  </Button>
                    </Form>
                    </div>
    )
}