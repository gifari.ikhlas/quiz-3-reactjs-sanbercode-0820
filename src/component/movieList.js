import React, { Component } from 'react';
import App from '../App.js';


import {
    Form,
    FormGroup,
    FormText,
    FormLabel,
    Button,
    Table
} from 'react-bootstrap';


let dataFilm = [
    {
        no:1,
        nama: "Avangers Infinity War",
        description: "Film ini bercerita tentang pahlawan super hero yang kuat ",
        year: 2018,
        duration:120,
        genre :"action",
        rating: 9,
        action: "a" 
        },
    {
        no: 1,
        nama: "Long Shot",
        description: "Film ini bercerita tentang pahlawan super hero yang kuat ",
        year: 2018,
        duration: 120,
        genre: "action",
        rating: 9,
        action : "a"
    },
  
]




    export const MovieList =()=>{
        return(
            <>
            <div style={{ width: "1000px", padding: "20px 20px", margin: "30px auto" }}>

                <h1>Daftar Movie</h1>
                <Table striped bordered hover>

                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Desciprtion</th>
                            <th>Year</th>
                            <th>Duration</th>
                            <th>Genre</th>
                            <th>Rating</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                       
                            <tr>
                                <td>1</td>
                                <td>Avangers Infinity War</td>
                                <td>Film ini bercerita tentang pahlawan super hero yang kuat</td>
                                <td>2018</td>
                                <td>120</td>
                                <td>action</td>
                                <td>9</td>
                                <td>
                                    <button type="button" className="btn btn-warning">Edit</button>
                                    <button type="button" className="btn btn-danger">Hapus</button>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Long Shot</td>
                                <td>Film ini bercerita tentang pahlawan super hero yang kuat</td>
                                <td>2020</td>
                                <td>120</td>
                                <td>comedy</td>
                                <td>8</td>
                                <td>
                                    <button type="button" className="btn btn-warning">Edit</button>
                                    <button type="button" className="btn btn-danger">Hapus</button>
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Maze Runner</td>
                                <td>Film ini bercerita tentang pahlawan super hero yang kuat</td>
                                <td>2013</td>
                                <td>120</td>
                                <td>action, Sci-fi</td>
                                <td>10</td>
                                <td>
                                    <button type="button" className="btn btn-warning">Edit</button>
                                    <button type="button" className="btn btn-danger">Hapus</button>
                                </td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Its Okay To not Be Cry</td>
                                <td>Film ini bercerita tentang pahlawan super hero yang kuat</td>
                                <td>2018</td>
                                <td>120</td>
                                <td>K=Drama</td>
                                <td>9</td>
                                <td>
                                    <button type="button" className="btn btn-warning">Edit</button>
                                    <button type="button" className="btn btn-danger">Hapus</button>
                                </td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>MULAN</td>
                                <td>Film ini bercerita tentang pahlawan super hero yang kuat</td>
                                <td>2020</td>
                                <td>120</td>
                                <td>action</td>
                                <td>9</td>
                                <td>
                                    <button type="button" className="btn btn-warning">Edit</button>
                                    <button type="button" className="btn btn-danger">Hapus</button>
                                </td>
                            </tr>

                        
                       

                    </tbody>
                </Table>
            </div>


            <div className="form" style={{width:"800px", border: "1px black solid",margin:"30px auto", padding: "20px 20px" }}>
                <Form text="left" >
                    <h1> Movie Form</h1>
                    <Form.Group controlId="formTitle">
                        <Form.Label for="title">Title </Form.Label>
                        <Form.Control id="title" type="text" placeholder="Enter title" />

                    </Form.Group>
                    <Form.Group controlId="formDescription">
                        <Form.Label for="Description">Description </Form.Label>
                        <Form.Control id="Description" as="textArea" placeholder="Enter Description" />

                    </Form.Group>
                    <Form.Group controlId="formYear">
                        <Form.Label for="Year">Year </Form.Label>
                        <Form.Control id="Year" type="text" placeholder="Enter Year" />

                    </Form.Group>
                    <Form.Group controlId="formDuration">
                        <Form.Label for="Duration">Duration </Form.Label>
                        <Form.Control id="Duration" type="text" placeholder="Enter Duration" />

                    </Form.Group>
                    <Form.Group controlId="formGenre">
                        <Form.Label for="Genre">Genre </Form.Label>
                        <Form.Control id="Genre" type="text" placeholder="Enter Genre" />

                    </Form.Group>
                    <Form.Group controlId="formRating">
                        <Form.Label for="Rating">Rating </Form.Label>
                        <Form.Control id="Rating" type="text" placeholder="Enter Rating" />
                    </Form.Group>

                    <Form.Group controlId="formImg">
                        <Form.Label for="Img">Image Url </Form.Label>
                        <Form.Control id="Img" as="textArea" placeholder="Enter Image Url" />

                    </Form.Group>






                    <Button variant="primary" type="submit">
                        Submit
  </Button>
                </Form>
                </div>
</>
        )
    }


        
