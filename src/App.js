
import React, {Component} from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import {Home} from './component/home.js';
import { MovieList } from './component/movieList.js';
import { Login } from './component/login.js';
import { About } from './component/about.js';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
   Navbar,
   NavbarBrand,
  NavbarToggle,
  NavbarCollapse,
  Nav,
  NavLink,
 

 } from 'react-bootstrap';
import { render } from '@testing-library/react';


 
export default class App extends Component {

  render(){
  return (
   
        <div className="App" >
   
  <Router>
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="#home">SanberCode</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              <Nav.Link href="/home">Home</Nav.Link>
              <Nav.Link href="/login">Login</Nav.Link>
              <Nav.Link href="/about">About</Nav.Link>
              <Nav.Link href="/movieList">Movie List</Nav.Link>
            </Nav>

          </Navbar.Collapse>
        </Navbar>
    <Switch>
         
          <Route exact path="/home" component={Home} />
          <Route path="/login" component={Login} />
          <Route path="/about" component={About} />
          <Route path="/movieList" component={MovieList} />

  
      </Switch>
</Router>
    </div>
  );
}
}



